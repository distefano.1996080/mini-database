#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "database.h"

//functions declaration

IndexNodeString *create_tree_string(char* root_value, Persona* persona);
IndexNodeInt *create_tree_int(int root_value, Persona* persona);
void insert_tree_string(IndexNodeString * root, char* s, Persona *persona);
void insert_tree_int(IndexNodeInt *root, int i, Persona *persona);
void free_tree_string(IndexNodeString *root);
void free_tree_int(IndexNodeInt *root);
Persona *find_tree_string(IndexNodeString *root, char *s);
Persona *find_tree_int(IndexNodeInt* root, int i);

//auxiliar function for the create_database one 
IndexNodeString *create_tree_string(char* root_value, Persona* persona){
    IndexNodeString* root = (IndexNodeString *)malloc(sizeof(IndexNodeString));
    if(root==NULL) return NULL;
    root->value = root_value;
    root->left = NULL;
    root->right = NULL;
    root->persona = persona;
    return root;
}

//the other auxiliar function for the create_database one
IndexNodeInt *create_tree_int(int root_value, Persona* persona){
    IndexNodeInt* root = (IndexNodeInt *)malloc(sizeof(IndexNodeInt));
    if(root==NULL) return NULL;
    root->value = root_value;
    root->left = NULL;
    root->right = NULL;
    root->persona = persona;
    return root;
}

//function that create a database from a person
Database *create_Database(Persona *persona){
    Database *root = (Database *) malloc(sizeof(Database));
    if(root==NULL) return NULL;
    root->name = create_tree_string(persona->name, persona);
    root->surname = create_tree_string(persona->surname, persona);
    root->address = create_tree_string(persona->address, persona);
    root->age = create_tree_int(persona->age, persona);
    return root;
}

//auxiliar function for the insert one
void insert_tree_string(IndexNodeString * root, char* s, Persona *persona){
    if(root==NULL) return;
    if(strcmp(s, root->value)<=0){
        if (root->left == NULL) {
            IndexNodeString *node = create_tree_string(s, persona);
            root->left = node;
            return;
        }
        insert_tree_string(root->left, s, persona);
        return;
    }
    if (root->right == NULL) {
        IndexNodeString *node = create_tree_string(s, persona);
        root->right = node;
        return;
    }
    insert_tree_string(root->right, s, persona);
}

//the other auxiliar function for the insert one
void insert_tree_int(IndexNodeInt *root, int i, Persona *persona){
    if(root==NULL) return;
    if(i <= root->value){
        if (root->left == NULL) {
            IndexNodeInt *node = create_tree_int(i, persona);
            root->left = node;
            return;
        }
        insert_tree_int(root->left,i, persona);
        return;
    }
    if (root->right == NULL) {
        IndexNodeInt *node = create_tree_int(i, persona);
        root->right = node;
        return;
    }
    insert_tree_int(root->right, i, persona);
}

//function that insert a person in the database
void insert(Database* database, Persona *persona){
    if(database==NULL) return;
    insert_tree_string(database->name, persona->name, persona);
    insert_tree_string(database->surname, persona->surname, persona);
    insert_tree_string(database->address, persona->address, persona);
    insert_tree_int(database->age, persona->age, persona);
    return;
}

//auxiliar function for the free_database one
void free_tree_string(IndexNodeString *root){
    if(root==NULL) return;
    free_tree_string(root->left);
    free_tree_string(root->right);
    free(root);
}

//the other auxiliar function for the free_database one
void free_tree_int(IndexNodeInt *root){
    if(root==NULL) return;
    free_tree_int(root->left);
    free_tree_int(root->right);
    free(root);
}

//function that work like a free but for databases
void free_database(Database *database){
    free_tree_string(database->name);
    free_tree_string(database->surname);
    free_tree_string(database->address);
    free_tree_int(database->age);
    free(database);
}

//auxiliar function for the find(s)
Persona *find_tree_string(IndexNodeString *root, char *s){
    if(strcmp(root->value, s)==0) return root->persona;
    if(root->left!=NULL){
        return find_tree_string(root->left, s);
    }
    if(root->right!=NULL){
        return find_tree_string(root->right, s);
    }
    return NULL;
}   

//the other auxiliar function for the find(s)
Persona *find_tree_int(IndexNodeInt* root, int i){
    if(root->value==i) return root->persona;
    if(root->left!=NULL){
        return find_tree_int(root->left, i);
    }
    if(root->right!=NULL){
        return find_tree_int(root->right, i);
    }
    return NULL;
}

//function that find a person from the name
Persona* findByName(Database * database, char * name){
    if(database==NULL) return NULL;
    return(find_tree_string(database->name, name));
}

//function that find a person from the surname
Persona* findBySurname(Database * database, char * surname){
    if(database==NULL) return NULL;
    return(find_tree_string(database->surname, surname));
}

//function that find a person from the address
Persona* findByAddress(Database * database, char * address){
    if(database==NULL) return NULL;
    return(find_tree_string(database->address, address));
}

//function that find a person from the age
Persona* findByAge(Database * database, int age){
    if(database==NULL) return NULL;
    return(find_tree_int(database->age, age));
}
