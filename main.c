#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "database.h"

Database *create_Database(Persona *persona);

Persona *create_persona(char* name, char*surname, char* address, int age){
    Persona * persona = (Persona *)malloc(sizeof(Persona));
    memcpy(persona->name, name, 20);
    memcpy(persona->surname, surname, 50);
    memcpy(persona->address, address, 100);
    persona->age = age;
    return persona;
}

void print_tree_string(IndexNodeString* root){
    if(root==NULL) return;
    print_tree_string(root->left);
    printf("%s\n", root->value);
    print_tree_string(root->right);
}

void print_tree_int(IndexNodeInt* root){
    if(root==NULL) return;
    print_tree_int(root->left);
    printf("%d\n", root->value);
    print_tree_int(root->right);
}

void print_database(Database * database){
    if(database==NULL) return;
    print_tree_string(database->name);
    print_tree_string(database->surname);
    print_tree_string(database->address);
    print_tree_int(database->age);
}

void free_database(Database *database);



int main(int argc, char **argv){
    Persona *p1 = create_persona("Jacopo", "Di Stefano", "Via Aldo Moro", 43);
    Persona *p2 = create_persona("Mastro", "Lindo", "Via kal", 33);
    Persona *p3 = create_persona("Gianfranco", "Buono", "Via Chiacchiera", 13);
    Persona *p4 = create_persona("Laura", "Pomeli", "Via Sicilia", 90);
    Database *d = create_Database(p3);
    printf("Creazione database con primo inserimento...\n");
    printf("Printing database...\n");
    print_database(d);
    
    printf("\nInserimento altri dati...\n");
    insert(d, p4);
    insert(d, p1);
    insert(d, p2);

    printf("\nPrinting database dopo inserimenti...\n");
    print_database(d);

    printf("\nCreating a new database, using all the people found in the first database...\n");
    Database *d2 = create_Database(findByName(d, "Gianfranco"));
    insert(d2, findBySurname(d, "Pomeli"));
    insert(d2, findByAddress(d, "Via Aldo Moro"));
    insert(d2, findByAge(d, 33));

    printf("\nPrinting it...\n");
    print_database(d2);

    free_database(d);
    free_database(d2);
    free(p1);
    free(p2);
    free(p3);
    free(p4);
    
    return 0;
}